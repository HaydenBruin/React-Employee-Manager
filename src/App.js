import React, { Fragment, Component } from 'react';

class App extends Component {
    render() {
        return (
            <Fragment>
                <div className="container">
                    <h1>Employee Manager</h1>

                    <table className="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Salary</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>1</th>
                                <td>Hayden</td>
                                <td>Bruin</td>
                                <td>$12,500</td>
                            </tr>
                            <tr>
                                <th>2</th>
                                <td>Shannon</td>
                                <td>Dawson</td>
                                <td>$14,000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </Fragment>
        );
    }
}

export default App;
